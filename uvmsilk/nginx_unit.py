from typing import List
# from uvmsilk.site_config import SiteConfig
from datetime import datetime
import pprint
import re
import json
import requests
import requests_unixsocket  # noqa F401
# from requests.exceptions import HTTPError
requests_unixsocket.monkeypatch()


class NginxUnitBase:
    """Base configuration for NGINX Unit"""

#    def __init__(self, socket_path: str, **kwargs) -> None:
#        self.socket_path = socket_path

    def _safe_name(self, hostname: str, uri: str = '/') -> str:
        safe_hostname = re.sub('[-.]', '_', hostname)
        if uri and uri != '/':
            if uri.endswith('/*'):
                uri = uri[:-2]
            safe_hostname += re.sub('[-./]', '_', uri)
        return safe_hostname


class NginxUnitListener():
    """Configuration for an NGINX Unit listener"""

    def __init__(self, network_port: int, ip: str = '%2A', passto: str = 'routes/hosts') -> None:
        self.network_port = network_port
        self.ip = ip
        self.passto = passto

    @property
    def uri(self):
        return '/listeners/{ip}:{port}'.format(ip=self.ip, port=self.network_port)

    @property
    def portinfo(self):
        return '{ip}:{port}'.format(ip=self.ip, port=self.network_port)

    def to_json(self):
        return {'pass': self.passto}


class NginxUnitRoute():
    """Configuration for an NGINX Unit route"""

    def __init__(self, match_hostname: str, **kwargs) -> None:
        self.match_hostname = match_hostname
        self.match_uri = '/'
        self.pass_action = None
        for attr in ['pass_action', 'match_uri', 'route_number']:
            setattr(self, attr, kwargs[attr] if attr in kwargs else None)
        if not self.match_uri.endswith('/*'):
            self.match_uri += '/*'
        self.match_uri = re.sub(r'/{2,}', '/', self.match_uri)

    @property
    def uri(self) -> str:
        return '/routes/host/{num}'.format(num=self.route_number)

    def to_json(self):
        relative_app_uri = self.pass_action
        data = {
            'match': {'host': self.match_hostname, 'uri': self.match_uri},
            'action': {'pass': relative_app_uri}
        }
        return data

class ApplicationBase(NginxUnitBase):
    """Base configuration for an NGINX Unit application"""

    def __init__(self, **kwargs) -> None:
        self.type = None
        self.version = kwargs.get('version')
        self.user = kwargs['user']
        if 'working_directory' in kwargs:
            self.working_directory = kwargs['working_directory']
        else:
            self.working_directory = kwargs.get('site_root')
        self.environment = {}
        for key, value in kwargs['environment'].items():
            self.environment[key.upper()] = value
        for env in ['SILK_APP_HOSTNAME', 'SILK_APP_URI']:
            self.environment[env] = kwargs['environment'][env] if env in kwargs['environment'] else None
        if self.environment['SILK_APP_HOSTNAME']:
            self.app_name = self._safe_name(self.environment['SILK_APP_HOSTNAME'], self.environment['SILK_APP_URI'])
        elif 'app_hostname' in kwargs:
            self.environment['SILK_APP_HOSTNAME'] = kwargs['app_hostname']
            self.environment['SILK_APP_URI'] = kwargs['app_uri'] if 'uri' in kwargs else '/'
            self.app_name = self._safe_name(self.environment['SILK_APP_HOSTNAME'], self.environment['SILK_APP_URI'])
        else:
            self.app_name = kwargs['app_name']
        if 'config_section' in kwargs:
            m = re.match(r'^app(:\s+)?(.*)', kwargs['config_section'])
            self.environment['SILK_APP_NAME'] = self.environment['SILK_APP_HOSTNAME'] if m.group(1) is None else m.group(1)

    @property
    def app_hostname(self) -> str:
        return self.environment['SILK_APP_HOSTNAME']

    @property
    def app_uri(self) -> str:
        return self.environment['SILK_APP_URI']

    @property
    def uri(self) -> str:
        return '/applications/{name}'.format(name=self.app_name)

    def to_json(self) -> dict:
        self.environment['SILK_APP_CONFIGURATION_TIME'] = datetime.now().isoformat()
        return {
            'type': '{type}{version}'.format(type=self.type, version=' ' + str(self.version) if self.version else ''),
            'user': self.user,
            'working_directory': self.working_directory,
            'environment': self.environment,
        }

    def application(**kwargs) -> 'ApplicationBase':
        if ' ' in kwargs['type']:
            kwargs['type'], kwargs['version'] = kwargs['type'].split(' ')
        type_map = {
            'external': ExternalApplication,  # as used in Unit
            'golang': ExternalApplication,    # as used in .silk.ini
            'java':   JavaApplication,
            'nodejs': ExternalApplication,    # as used in .silk.ini
            'python': PythonApplication,
            'ruby':   RubyApplication,
        }
        return type_map[kwargs['type']](**kwargs)


class ExternalApplication(ApplicationBase):
    """Configuration for a Go or Node.js application"""

    def __init__(self,  **kwargs) -> None:
        super().__init__(**kwargs)
        self.type = 'external'
        self.executable = kwargs['executable']

    def to_json(self) -> dict:
        json = super().to_json()
        json['executable'] = self.executable
        return json


class JavaApplication(ApplicationBase):
    """Configuration for a Java application"""

    def __init__(self,  **kwargs) -> None:
        super().__init__(**kwargs)
        self.type = 'java'
        if 'webapp' in kwargs:
            self.webapp = kwargs['webapp']
        elif 'document-root' in kwargs and kwargs['document-root'] != '':
            self.webapp = kwargs['document-root']
        else:
            self.webapp = self.working_directory + '/' + kwargs['executable']

    def to_json(self) -> dict:
        json = super().to_json()
        json['webapp'] = self.webapp
        del json['working_directory']  # TODO Check if this is supposed to work
        return json


class RubyApplication(ApplicationBase):
    """Configuration for a Ruby application"""

    def __init__(self,  **kwargs) -> None:
        super().__init__(**kwargs)
        self.type = 'ruby'
        if 'script' in kwargs:
            self.executable = kwargs['script']
        else:
            self.executable = kwargs['executable']

    def to_json(self) -> dict:
        json = super().to_json()
        json['script'] = self.executable
        return json


class PythonApplication(ApplicationBase):
    """Configuration for a Python application"""

    def __init__(self,  **kwargs) -> None:
        super().__init__(**kwargs)
        self.type = 'python'
        if 'module' in kwargs:
            self.module = kwargs['module']
        else:
            self.module = kwargs['executable']
            if self.module.endswith('.py'):
                # It is more intutive to configure the startup module
                # as a script name, but Unit imports it as a module.
                self.module = self.module[:-3]

    def to_json(self) -> dict:
        json = super().to_json()
        json['path'] = self.working_directory
        json['module'] = self.module
        return json


class NginxUnit(NginxUnitBase):
    """Configuration for NGINX Unit app server."""

    def __init__(self, socket_path: str, **kwargs) -> None:
        self.socket_path = socket_path.replace('/', '%2F')
        if 'network_port' in kwargs:
            self.network_port = kwargs['network_port']
        else:
            self.network_port = None
        self._listeners = {}
        self._applications = []
        self._routes = {}

    def _get_json(self, config_uri):
        r = requests.get('http+unix://{socket_path}/config{config_uri}'.format(socket_path=self.socket_path, config_uri=config_uri))
        r.raise_for_status()
        return r.json()

    def _post_json(self, config_uri, data) -> int:
        r = requests.post('http+unix://{socket_path}/config{config_uri}'.format(socket_path=self.socket_path, config_uri=config_uri), data=json.dumps(data))
        r.raise_for_status()
        return r.status_code

    def _put_json(self, config_uri, data) -> int:
        r = requests.put('http+unix://{socket_path}/config{config_uri}'.format(socket_path=self.socket_path, config_uri=config_uri), data=json.dumps(data))
        r.raise_for_status()
        return r.status_code

    def minimal_config(self) -> str:
        # FIXME: This should:
        #        1. Stop the running server.
        #        2. Delete the current configuration.
        #        3. Replace the current configuration.
        #        4. Start the server.
        return {
            "listeners": {
                "*:{port}": "routes/hosts".format(port=self.network_port)
            },
            "routes": {
                "hosts": []
            },
            "applications": {}
        }

    def _build_listeners(self):
        if not self._listeners:
            self._listeners = {}
            for portinfo, data in self._get_json('/listeners').items():
                ip, port = portinfo.split(':')
                self._listeners[portinfo] = NginxUnitListener(port, ip, data['pass'])

    @property
    def listeners(self):
        self._build_listeners()
        return self._listeners

    def listener(self, network_port: int) -> NginxUnitListener:
        self._build_listeners()
        return self._listeners.get('*:{port}'.format(port=network_port))

    def save_listener(self, l: NginxUnitListener) -> None:
        self._build_listeners()
        self._put_json(l.uri, l.to_json())

#        needed_updating = False
#        if (
#             not self._listeners or
#             l.portinfo not in self._listeners or
#             DeepDiff(self._listeners[l.portinfo], l, ignore_order=True)
#           ):
#            needed_updating = True
#            self._put_json('/listeners/{name}'.format(name=l.portinfo), l.to_json())
#        return needed_updating

    def _build_routes(self) -> None:
        if not self._routes:
            self._routes = {}
            try:
                for idx, data in enumerate(self._get_json('/routes/hosts')):
                    app_name = data['action']['pass'].split('/')[1]
                    self._routes[app_name] = NginxUnitRoute(
                            match_hostname=data['match']['host'],
                            match_uri=data['match'].get('uri'),
                            pass_action=data['action']['pass'],
                            route_number=idx)
            except:
                pass

    def routes(self) -> str:
        self._build_routes()
        return self._routes

    def route(self, hostname: str, uri: str = '/') -> NginxUnitRoute:
        self._build_routes()
        return self._routes.get(self._safe_name(hostname, uri))

    def save_route(self, route: NginxUnitRoute) -> None:
        self._build_routes()

        try:
            routes = self._get_json('/routes/hosts')
        except:
            self._put_json('/routes', {'hosts': []})

        if route.route_number:
            self._put_json(route.uri, route.to_json())
        else:
            app_name = self._safe_name(route.match_hostname, route.match_uri)
            route.pass_action = 'applications/' + self._safe_name(route.match_hostname, route.match_uri)
            self._post_json('/routes/hosts', route.to_json())
            self._routes = None  # force data reload
            self._build_routes()
            route.route_number = self._routes[app_name].route_number

    def _build_applications(self) -> None:
        if not self._applications:
            self._applications = {}
            for app_name, data in self._get_json('/applications').items():
                self._applications[app_name] = ApplicationBase.application(app_name=app_name, **data)

    def applications(self) -> List[ApplicationBase]:
        self._build_applications()
        return self._applications.values()

    def application(self, hostname: str, uri: str = '/', **kwargs) -> ApplicationBase:
        self._build_applications()
        return self._applications.get(self._safe_name(hostname, uri))

    def save_application(self, app: ApplicationBase) -> None:
        self._build_applications()
        self._put_json(app.uri, app.to_json())
